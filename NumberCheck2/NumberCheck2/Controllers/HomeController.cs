﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExcelDataReader;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NumberCheck2.Models;

namespace NumberCheck2.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Test()
        {
            var clasa = new MainClass();
            clasa.Principal();
            return View(clasa.test1);
        }

        public IActionResult Privacy()
        {
            return View();
        }







    }
}
