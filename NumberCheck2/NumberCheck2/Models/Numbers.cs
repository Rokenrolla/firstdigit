﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NumberCheck2.Models
{
    public class Numbers
    {
        public int One { get; set; }
        public int Two { get; set; }
        public int Tree { get; set; }
        public int Four { get; set; }
        public int Five { get; set; }
        public int Six { get; set; }
        public int Seven { get; set; }
        public int Eight { get; set; }
        public int Nine { get; set; }
    }
}
