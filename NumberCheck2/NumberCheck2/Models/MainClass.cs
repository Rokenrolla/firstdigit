﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NumberCheck2.Models
{
    public class MainClass
    {
        public Numbers test1 = new Numbers();
        public void Principal()
        {
            List<int> AddAllDigits = new List<int>();
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = File.Open(@"C:\Users\User\Desktop\test.xlsx", FileMode.Open, FileAccess.Read)) //change your filepath here
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    do
                    {
                        while (reader.Read()) //Each ROW
                        {
                            for (int column = 0; column < reader.FieldCount; column++)
                            {
                                if (Convert.ToInt32(reader.GetValue(column)) != 0)
                                    AddAllDigits.Add(Convert.ToInt32(reader.GetValue(column)));
                            }
                        }
                    } while (reader.NextResult()); //checks all sheets
                }
            }

            int[] balance = new int[9];
            for (int i = 0; i < 9; i++)
            {
                balance[i] = 0;
            }

            foreach (var x in AddAllDigits)
            {
                for (int i = 0; i < 9; i++)
                {
                    if (firstDigit(x) == i + 1) { balance[i]++; }
                }
            }


            
            List<int> ReturnDigits = new List<int>();
            for (int i = 0; i < 9; i++)
            {
                test1.One = balance[0];
                test1.Two = balance[1];
                test1.Tree = balance[2];
                test1.Four = balance[3];
                test1.Five = balance[4];
                test1.Six = balance[5];
                test1.Seven = balance[6];
                test1.Eight = balance[7];
                test1.Nine = balance[8];
            }
        }


        public static int firstDigit(int n)
        {
            while (n >= 10)
                n /= 10;
            return n;
        }
    }
}
